public class Board {
  
  private Die p1;
  private Die p2;
  private int[][] closedTiles;
  private final int LIMIT = 3;
  private int x = 0;
  
  public Board () {
   this.p1 = new Die();
   this.p2 = new Die();
   this.closedTiles = new int[6][6];
  }
  public String toString() {
   String cT = "";
   for (int i = 0; i < this.closedTiles.length; i++) {
	   for (int j = 0; j < this.closedTiles[i].length; j++) {
		   if (j + 1 == 6) {
			   cT += "(" + (i+1) + "," + (j+1) + ")" + "Times Played : " + this.closedTiles[i][j] + "\n";
		   } else {
			   cT += "(" + (i+1) + "," + (j+1) + ")" + "Times Played : " + this.closedTiles[i][j] + " | ";
		   }
		   }
   }
   return cT;
  }
 
 public boolean playATurn () {
   this.p1.roll();
   this.p2.roll();
   System.out.println(this.p1.getPips());
   System.out.println(this.p2.getPips());
   int sumOfPips = this.p1.getPips() + this.p2.getPips();
   
   this.closedTiles[this.p1.getPips()-1][this.p2.getPips()-1]++;
   
   if (this.closedTiles[this.p1.getPips()-1][this.p2.getPips()-1] == this.LIMIT) {
	   System.out.println("The tile at this position has already been shut " +this.LIMIT+ "times!");
	   return true;
	   } else {
	   System.out.println("Closing tile: "+sumOfPips);
	   return false;
   }
   }
}
 